# fixrun - FInd eXecutables and RUN

`fixrun` runs tests.
It finds all the executables in the `test` directory and
runs each one of them.

    fixrun [-out] [-err]

A report is produced on `stdout`.

The exit status is 0 when all tests pass.

The exit status is 1 when some tests fail.

An error condition results in exit status 2.

The `-out` and `-err` flags make the stdout and stderr visible;
normally they are discarded.

## Discovery

Starting with the directory `test`,
`fixrun` recurses the file system to discover tests to run.
It attempts to run any file that it finds,
setting the current directory to the directory containing that file.
An error when attempting to run a file is ignored;
it is assumed to be a non-executable file, like a data file.

# END
