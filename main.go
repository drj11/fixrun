package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"

	"reflect"
)

var outp = flag.Bool("out", false, "show stdout of subprocesses")
var errp = flag.Bool("err", false, "show stderr of subprocesses")

func main() {

	flag.Parse()

	start := "test"
	visit, score := makeWalker(*outp, *errp)

	err := filepath.Walk(start, visit)
	tried, ok, erred := score()
	failed := tried - ok - erred

	fmt.Println()
	fmt.Printf("%d/%d tests pass\n", ok, tried)
	fmt.Printf("%d/%d tests fail\n", failed, tried)
	if err != nil {
		log.Fatal(err)
	}
	switch {
	case erred == 0 && failed > 0:
		os.Exit(1)
	case erred > 0:
		os.Exit(2)
	}
}

// Return a path walking function (can be passed to filepath.Walk),
// and a separate function that returns a score of (tried, ok, erred).
func makeWalker(stdout, stderr bool,
) (func(string, os.FileInfo, error) error, func() (int, int, int)) {
	tried := 0
	ok := 0
	erred := 0

	s := func() (int, int, int) {
		return tried, ok, erred
	}

	v := func(path string, info os.FileInfo, err error) error {
		dir := filepath.Dir(path)
		base := filepath.Base(path)
		cmd := exec.Command("./" + base)
		cmd.Dir = dir

		if stdout {
			cmd.Stdout = os.Stdout
		}
		if stderr {
			cmd.Stderr = os.Stderr
		}

		err = cmd.Run()

		switch e := err.(type) {
		case nil:
			fmt.Println("  \033[32m✓\033[m", path)
			tried += 1
			ok += 1
		case *exec.ExitError:
			// Finished with non-zero exit status.
			fmt.Println("  \033[31m✗\033[m", path)
			tried += 1
		case *os.PathError:
			// A non-executable file.
		default:
			tried += 1
			erred += 1
			fmt.Println("  \033[31m✗\033[m", path, reflect.TypeOf(e), e)
		}

		return nil
	}
	return v, s
}
